ALTER TABLE "COVID$COLOR" ADD "STORE" nvarchar2(20) NULL;
INSERT INTO "MENDIXSYSTEM$ATTRIBUTE" ("ID", 
"ENTITY_ID", 
"ATTRIBUTE_NAME", 
"COLUMN_NAME", 
"TYPE", 
"LENGTH", 
"DEFAULT_VALUE", 
"IS_AUTO_NUMBER")
 VALUES ('fd7f27c0-65c7-4520-a53e-f031b511b604', 
'3dcff867-18c1-4add-8246-932eaa3b8954', 
'store', 
'STORE', 
30, 
20, 
'', 
0);
ALTER TABLE "COVID$QUESTION" ADD "STORE" nvarchar2(20) NULL;
INSERT INTO "MENDIXSYSTEM$ATTRIBUTE" ("ID", 
"ENTITY_ID", 
"ATTRIBUTE_NAME", 
"COLUMN_NAME", 
"TYPE", 
"LENGTH", 
"DEFAULT_VALUE", 
"IS_AUTO_NUMBER")
 VALUES ('212f6e15-540c-4878-a40f-d1da36149e8a', 
'a8661b37-1dd2-4344-a040-730e7a9bfa10', 
'store', 
'STORE', 
30, 
20, 
'', 
0);
ALTER TABLE "COVID$TEXTMASTER" ADD "STORE" nvarchar2(20) NULL;
INSERT INTO "MENDIXSYSTEM$ATTRIBUTE" ("ID", 
"ENTITY_ID", 
"ATTRIBUTE_NAME", 
"COLUMN_NAME", 
"TYPE", 
"LENGTH", 
"DEFAULT_VALUE", 
"IS_AUTO_NUMBER")
 VALUES ('6645ed74-b1c1-4e91-b38f-4381475621ed', 
'dd6f956d-8cee-4f7e-b3a2-a2a22ac85d50', 
'store', 
'STORE', 
30, 
20, 
'', 
0);
ALTER TABLE "COVID$PROFILEINFO" ADD "PROFILETYPEHO" nvarchar2(38) NULL;
ALTER TABLE "COVID$PROFILEINFO" ADD "DEPARTMENT" nvarchar2(35) NULL;
INSERT INTO "MENDIXSYSTEM$ATTRIBUTE" ("ID", 
"ENTITY_ID", 
"ATTRIBUTE_NAME", 
"COLUMN_NAME", 
"TYPE", 
"LENGTH", 
"DEFAULT_VALUE", 
"IS_AUTO_NUMBER")
 VALUES ('67a8c4ca-aa5f-4af0-bda4-a9003898b371', 
'e66853f7-5428-4238-81f9-ff3b1b9ed5fa', 
'department', 
'DEPARTMENT', 
40, 
35, 
'', 
0);
INSERT INTO "MENDIXSYSTEM$ATTRIBUTE" ("ID", 
"ENTITY_ID", 
"ATTRIBUTE_NAME", 
"COLUMN_NAME", 
"TYPE", 
"LENGTH", 
"DEFAULT_VALUE", 
"IS_AUTO_NUMBER")
 VALUES ('41f6f564-7e87-4f8c-962e-1ee4ce86d0d7', 
'e66853f7-5428-4238-81f9-ff3b1b9ed5fa', 
'ProfileTypeHO', 
'PROFILETYPEHO', 
40, 
38, 
'', 
0);
UPDATE "MENDIXSYSTEM$VERSION"
 SET "VERSIONNUMBER" = '4.2', 
"LASTSYNCDATE" = timestamp'2021-03-31 10:21:14';
