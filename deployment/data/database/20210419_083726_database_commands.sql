ALTER TABLE "ADMINISTRATION$ACCOUNT" RENAME TO "651880D1A0394128A6740E4B3B4486";
DELETE FROM "MENDIXSYSTEM$ENTITY" 
 WHERE "ID" = 'e0aa2640-a77a-4e83-acbb-958c17dda911';
DELETE FROM "MENDIXSYSTEM$ENTITYIDENTIFIER" 
 WHERE "ID" = 'e0aa2640-a77a-4e83-acbb-958c17dda911';
DELETE FROM "MENDIXSYSTEM$SEQUENCE" 
 WHERE "ATTRIBUTE_ID" IN (SELECT "ID"
 FROM "MENDIXSYSTEM$ATTRIBUTE"
 WHERE "ENTITY_ID" = 'e0aa2640-a77a-4e83-acbb-958c17dda911');
DELETE FROM "MENDIXSYSTE$REMOTE_PRIMARY_KEY" 
 WHERE "ENTITY_ID" = 'e0aa2640-a77a-4e83-acbb-958c17dda911';
DELETE FROM "MENDIXSYSTEM$ATTRIBUTE" 
 WHERE "ENTITY_ID" = 'e0aa2640-a77a-4e83-acbb-958c17dda911';
DELETE FROM "SYSTEM$USER" 
 WHERE "ID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "SYSTEM$USER_TIMEZONE" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "SYSTEM$USERROLES" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "SYSTEM$USER_LANGUAGE" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$ATTRIBUTE"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$ATTRIBUTE"
 SET "SYSTEM$CHANGEDBY" = NULL
 WHERE "SYSTEM$CHANGEDBY" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$ENTITY"
 SET "SYSTEM$CHANGEDBY" = NULL
 WHERE "SYSTEM$CHANGEDBY" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$ENTITY"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$PENDINGLINK"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$PENDINGLINK"
 SET "SYSTEM$CHANGEDBY" = NULL
 WHERE "SYSTEM$CHANGEDBY" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$MICROFLOW"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$MICROFLOW"
 SET "SYSTEM$CHANGEDBY" = NULL
 WHERE "SYSTEM$CHANGEDBY" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$DEEPLINK"
 SET "SYSTEM$CHANGEDBY" = NULL
 WHERE "SYSTEM$CHANGEDBY" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "DEEPLINK$DEEPLINK"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "SYSTEM$FILEDOCUMENT"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "SYSTEM$FILEDOCUMENT"
 SET "SYSTEM$CHANGEDBY" = NULL
 WHERE "SYSTEM$CHANGEDBY" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
UPDATE "SYSTEM$SYNCHRONIZATIONERROR"
 SET "SYSTEM$OWNER" = NULL
 WHERE "SYSTEM$OWNER" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "SYSTEM$SESSION_USER" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "MENDIXSSO$FOREIGNIDENTITY_USER" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "MENDIXSSO$TOKEN_USER" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "SYSTEM$USERREPORTINFO_USER" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DELETE FROM "SYSTEM$TOKENINFORMATION_USER" 
 WHERE "SYSTEM$USERID" IN (SELECT "ID"
 FROM "651880D1A0394128A6740E4B3B4486");
DROP TABLE "651880D1A0394128A6740E4B3B4486";
UPDATE "MENDIXSYSTEM$VERSION"
 SET "VERSIONNUMBER" = '4.2', 
"LASTSYNCDATE" = timestamp'2021-04-19 08:37:23';
