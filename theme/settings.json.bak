{
  "pageTemplates": "WebModeler",
  "cssFiles": [
    "styles/css/lib/lib.css",
    "styles/css/custom/custom.css"
  ],
  "designProperties": {
    "Widget": [
      {
        "name": "Spacing top",
        "type": "Dropdown",
        "description": "The spacing above this element.",
        "options": [
          {
            "name": "None",
            "class": "spacing-outer-top-none"
          },
          {
            "name": "Small",
            "class": "spacing-outer-top"
          },
          {
            "name": "Medium",
            "class": "spacing-outer-top-medium"
          },
          {
            "name": "Large",
            "class": "spacing-outer-top-large"
          },
          {
            "name": "Layout",
            "class": "spacing-outer-top-layout"
          }
        ]
      },
      {
        "name": "Spacing bottom",
        "type": "Dropdown",
        "description": "The spacing below this element.",
        "options": [
          {
            "name": "None",
            "class": "spacing-outer-bottom-none"
          },
          {
            "name": "Small",
            "class": "spacing-outer-bottom"
          },
          {
            "name": "Medium",
            "class": "spacing-outer-bottom-medium"
          },
          {
            "name": "Large",
            "class": "spacing-outer-bottom-large"
          },
          {
            "name": "Layout",
            "class": "spacing-outer-bottom-layout"
          }
        ]
      },
      {
        "name": "Spacing left",
        "type": "Dropdown",
        "description": "The spacing to the left of this element.",
        "options": [
          {
            "name": "None",
            "class": "spacing-outer-left-none"
          },
          {
            "name": "Small",
            "class": "spacing-outer-left"
          },
          {
            "name": "Medium",
            "class": "spacing-outer-left-medium"
          },
          {
            "name": "Large",
            "class": "spacing-outer-left-large"
          },
          {
            "name": "Layout",
            "class": "spacing-outer-left-layout"
          }
        ]
      },
      {
        "name": "Spacing right",
        "type": "Dropdown",
        "description": "The spacing to the right of this element.",
        "options": [
          {
            "name": "None",
            "class": "spacing-outer-right-none"
          },
          {
            "name": "Small",
            "class": "spacing-outer-right"
          },
          {
            "name": "Medium",
            "class": "spacing-outer-right-medium"
          },
          {
            "name": "Large",
            "class": "spacing-outer-right-large"
          },
          {
            "name": "Layout",
            "class": "spacing-outer-right-layout"
          }
        ]
      },
      {
        "name": "Full height",
        "type": "Toggle",
        "description": "Make the height 100%",
        "class": "h-100"
      },
      {
        "name": "Full width",
        "type": "Toggle",
        "description": "Make the width 100%",
        "class": "w-100"
      },
      {
        "name": "Border right",
        "type": "Toggle",
        "description": "Add a border to the right",
        "class": "border-right"
      },
      {
        "name": "Border left",
        "type": "Toggle",
        "description": "Add a border to the left",
        "class": "border-left"
      },
      {
        "name": "Border top",
        "type": "Toggle",
        "description": "Add a border to the top",
        "class": "border-top"
      },
      {
        "name": "Border bottom",
        "type": "Toggle",
        "description": "Add a border to the bottom",
        "class": "border-bottom"
      },
      {
        "name": "Align self",
        "oldNames": [
          "Align Self"
        ],
        "type": "Dropdown",
        "description": "Float the element to the left or to the right.",
        "options": [
          {
            "name": "Left",
            "class": "pull-left"
          },
          {
            "name": "Right",
            "class": "pull-right"
          }
        ]
      },
      {
        "name": "Hide on phone",
        "oldNames": [
          "Hide On Phone"
        ],
        "type": "Toggle",
        "description": "Hide element on phone.",
        "class": "hide-phone"
      },
      {
        "name": "Hide on tablet",
        "oldNames": [
          "Hide On Tablet"
        ],
        "type": "Toggle",
        "description": "Hide element on tablet.",
        "class": "hide-tablet"
      },
      {
        "name": "Hide on desktop",
        "oldNames": [
          "Hide On Desktop"
        ],
        "type": "Toggle",
        "description": "Hide element on desktop.",
        "class": "hide-desktop"
      }
    ],
    "DivContainer": [
      {
        "name": "Align content",
        "type": "Dropdown",
        "description": "Align content of this element left, right or center it. Align elements inside the container as a row or as a column.",
        "options": [
          {
            "name": "Left align as a row",
            "oldNames": [
              "Left align as row"
            ],
            "class": "row-left"
          },
          {
            "name": "Center align as row and vertical align left",
            "class": "row-center-start"
          },
          {
            "name": "Center align as a row",
            "oldNames": [
              "Center align as row"
            ],
            "class": "row-center"
          },
          {
            "name": "Right align as a row",
            "oldNames": [
              "Right align as row"
            ],
            "class": "row-right"
          },
          {
            "name": "Left align as a column",
            "oldNames": [
              "Left align as column"
            ],
            "class": "col-left"
          },
          {
            "name": "Center align as a column",
            "oldNames": [
              "Center align as column"
            ],
            "class": "col-center"
          },
          {
            "name": "Right align as a column",
            "oldNames": [
              "Right align as column"
            ],
            "class": "col-right"
          }
        ]
      },
      {
        "name": "Current step",
        "type": "Toggle",
        "description": "Show that this is the current step",
        "class": "current"
      },
      {
        "name": "Complete step",
        "type": "Toggle",
        "description": "Show that this step is complete",
        "class": "complete"
      },
      {
        "name": "Flex wrap",
        "type": "Toggle",
        "description": "Wrap the flexcontainer",
        "class": "flex-wrap"
      },
      {
        "name": "Background color",
        "type": "Dropdown",
        "description": "Change the background color of the container.",
        "options": [
          {
            "name": "Brand Default",
            "oldNames": [
              "Default"
            ],
            "class": "background-default"
          },
          {
            "name": "Brand Primary",
            "oldNames": [
              "Primary"
            ],
            "class": "background-primary"
          },
          {
            "name": "Brand Inverse",
            "oldNames": [
              "Inverse"
            ],
            "class": "background-inverse"
          },
          {
            "name": "Brand Info",
            "oldNames": [
              "Info"
            ],
            "class": "background-info"
          },
          {
            "name": "Brand Success",
            "oldNames": [
              "Success"
            ],
            "class": "background-success"
          },
          {
            "name": "Brand Warning",
            "oldNames": [
              "Warning"
            ],
            "class": "background-warning"
          },
          {
            "name": "Brand Danger",
            "oldNames": [
              "Danger"
            ],
            "class": "background-danger"
          },
          {
            "name": "Background Default",
            "class": "background-main"
          },
          {
            "name": "Background Dashboard",
            "class": "background-secondary"
          }
        ]
      }
    ],
    "Button": [
      {
        "name": "Size",
        "type": "Dropdown",
        "description": "Size of the buttons",
        "options": [
          {
            "name": "Small",
            "class": "btn-sm"
          },
          {
            "name": "Large",
            "class": "btn-lg"
          }
        ]
      },
      {
        "name": "Full width",
        "type": "Toggle",
        "description": "Extend the button to the full width of the container it is placed in.",
        "class": "btn-block"
      },
      {
        "name": "Border",
        "oldNames": [
          "Bordered"
        ],
        "type": "Toggle",
        "description": "Style the button with a transparent background, a colored border, and colored text.",
        "class": "btn-bordered"
      },
      {
        "name": "Rounded",
        "type": "Toggle",
        "description": "Buttons styled with rounded edges.",
        "class": "btn-rounded"
      },
      {
        "name": "Align icon",
        "type": "Dropdown",
        "description": "Place the icon to the left or on top of the button.",
        "options": [
          {
            "name": "Right",
            "class": "btn-icon-right"
          },
          {
            "name": "Top",
            "class": "btn-icon-top"
          }
        ]
      }
    ],
    "ListView": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Change the appearance of rows in the list view.",
        "options": [
          {
            "name": "Striped",
            "class": "listview-striped"
          },
          {
            "name": "Bordered",
            "class": "listview-bordered"
          },
          {
            "name": "Lined",
            "class": "listview-lined"
          },
          {
            "name": "No Styling",
            "class": "listview-stylingless"
          }
        ]
      },
      {
        "name": "Hover style",
        "type": "Toggle",
        "description": "Highlight a row when hovering over it. Only useful when the row is clickable.",
        "class": "listview-hover"
      },
      {
        "name": "Row Size",
        "type": "Dropdown",
        "description": "Change the row spacing of the list view.",
        "options": [
          {
            "name": "Small",
            "class": "listview-sm"
          },
          {
            "name": "Large",
            "class": "listview-lg"
          }
        ]
      }
    ],
    "DataGrid": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Choose one of the following styles to change the appearance of the data grid.",
        "options": [
          {
            "name": "Striped",
            "class": "datagrid-striped"
          },
          {
            "name": "Bordered",
            "class": "datagrid-bordered"
          },
          {
            "name": "Lined",
            "class": "datagrid-lined"
          }
        ]
      },
      {
        "name": "Hover style",
        "type": "Toggle",
        "description": "Enable data grid hover to make the rows hoverable.",
        "class": "datagrid-hover"
      },
      {
        "name": "Row size",
        "type": "Dropdown",
        "description": "Increase or decrease the row spacing of the data grid row.",
        "options": [
          {
            "name": "Small",
            "class": "datagrid-sm"
          },
          {
            "name": "Large",
            "class": "datagrid-lg"
          }
        ]
      }
    ],
    "TemplateGrid": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Choose one of the following styles to change the appearance of the template grid.",
        "options": [
          {
            "name": "Striped",
            "class": "templategrid-striped"
          },
          {
            "name": "Bordered",
            "class": "templategrid-bordered"
          },
          {
            "name": "Lined",
            "class": "templategrid-lined"
          },
          {
            "name": "No styling",
            "class": "templategrid-stylingless"
          }
        ]
      },
      {
        "name": "Hover style",
        "type": "Toggle",
        "description": "Enable template grid hover to make the rows hoverable.",
        "class": "templategrid-hover"
      }
    ],
    "GroupBox": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Choose one of the following styles to change the appearance of the groupbox.",
        "options": [
          {
            "name": "Brand Default",
            "oldNames": [
              "Default"
            ],
            "class": "groupbox-default"
          },
          {
            "name": "Brand Primary",
            "oldNames": [
              "Primary"
            ],
            "class": "groupbox-primary"
          },
          {
            "name": "Brand Inverse",
            "oldNames": [
              "Inverse"
            ],
            "class": "groupbox-inverse"
          },
          {
            "name": "Brand Info",
            "oldNames": [
              "Info"
            ],
            "class": "groupbox-info"
          },
          {
            "name": "Brand Success",
            "oldNames": [
              "Success"
            ],
            "class": "groupbox-success"
          },
          {
            "name": "Brand Warning",
            "oldNames": [
              "Warning"
            ],
            "class": "groupbox-warning"
          },
          {
            "name": "Brand Danger",
            "oldNames": [
              "Danger"
            ],
            "class": "groupbox-danger"
          },
          {
            "name": "Transparent",
            "class": "groupbox-transparent"
          }
        ]
      },
      {
        "name": "Callout style",
        "type": "Toggle",
        "description": "Enable the groupbox callout functionality to highlight the importance of the groupbox.",
        "class": "groupbox-callout"
      }
    ],
    "StaticImageViewer": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Choose the style of your image.",
        "options": [
          {
            "name": "Rounded",
            "class": "img-rounded"
          },
          {
            "name": "Thumbnail",
            "class": "img-thumbnail"
          },
          {
            "name": "Circle",
            "class": "img-circle"
          }
        ]
      },
      {
        "name": "Profile image",
        "type": "Dropdown",
        "description": "Choose the size of your profile image.",
        "options": [
          {
            "name": "Small",
            "class": "img-profile-sm"
          },
          {
            "name": "Medium",
            "class": "img-profile-md"
          },
          {
            "name": "Large",
            "class": "img-profile-lg"
          }
        ]
      },
      {
        "name": "Center",
        "type": "Toggle",
        "description": "Align the image in the center of an element.",
        "class": "img-center"
      },
      {
        "name": "Filter darken",
        "type": "Toggle",
        "description": "Darken the image",
        "class": "filter-darken"
      },
      {
        "name": "Filter blur",
        "type": "Toggle",
        "description": "Blue the image",
        "class": "filter-blur"
      },
      {
        "name": "Add border",
        "type": "Toggle",
        "description": "Add a border to the image.",
        "class": "img-border"
      },
      {
        "name": "Add shadow",
        "type": "Toggle",
        "description": "Add a shadow to the image.",
        "class": "img-boxshadow"
      },
      {
        "name": "Image fit",
        "type": "Dropdown",
        "description": "Specify how the image should be resized to fit its container.",
        "options": [
          {
            "name": "Fit cover",
            "class": "fit-cover"
          },
          {
            "name": "Fit contain",
            "class": "fit-contain"
          },
          {
            "name": "Fit fill",
            "class": "fit-fill"
          },
          {
            "name": "Fit none",
            "class": "fit-none"
          }
        ]
      },
      {
        "name": "Image position",
        "type": "Dropdown",
        "description": "Defines how the image is positioned.",
        "options": [
          {
            "name": "Left top",
            "class": "position-left-top"
          },
          {
            "name": "Left center",
            "class": "position-left-center"
          },
          {
            "name": "Left bottom",
            "class": "position-left-bottom"
          },
          {
            "name": "Right top",
            "class": "position-right-top"
          },
          {
            "name": "Right center",
            "class": "position-right-center"
          },
          {
            "name": "Right bottom",
            "class": "position-right-bottom"
          },
          {
            "name": "Center top",
            "class": "position-center-top"
          },
          {
            "name": "Center center",
            "class": "position-center-center"
          },
          {
            "name": "Center bottom",
            "class": "position-center-bottom"
          }
        ]
      }
    ],
    "DynamicImageViewer": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Choose the style of your image.",
        "options": [
          {
            "name": "Rounded",
            "class": "img-rounded"
          },
          {
            "name": "Thumbnail",
            "class": "img-thumbnail"
          },
          {
            "name": "Circle",
            "class": "img-circle"
          }
        ]
      },
      {
        "name": "Profile image",
        "type": "Dropdown",
        "description": "Choose the size of your profile image.",
        "options": [
          {
            "name": "Small",
            "class": "img-profile-sm"
          },
          {
            "name": "Medium",
            "class": "img-profile-md"
          },
          {
            "name": "Large",
            "class": "img-profile-lg"
          }
        ]
      },
      {
        "name": "Center",
        "type": "Toggle",
        "description": "Align the image in the center of an element.",
        "class": "img-center"
      },
      {
        "name": "Filter darken",
        "type": "Toggle",
        "description": "Darken the image",
        "class": "filter-darken"
      },
      {
        "name": "Filter blur",
        "type": "Toggle",
        "description": "Blue the image",
        "class": "filter-blur"
      },
      {
        "name": "Add border",
        "type": "Toggle",
        "description": "Add a border to the image.",
        "class": "img-border"
      },
      {
        "name": "Add shadow",
        "type": "Toggle",
        "description": "Add a shadow to the image.",
        "class": "img-boxshadow"
      },
      {
        "name": "Image fit",
        "type": "Dropdown",
        "description": "Specify how the image should be resized to fit its container.",
        "options": [
          {
            "name": "Fit cover",
            "class": "fit-cover"
          },
          {
            "name": "Fit contain",
            "class": "fit-contain"
          },
          {
            "name": "Fit fill",
            "class": "fit-fill"
          },
          {
            "name": "Fit none",
            "class": "fit-none"
          }
        ]
      },
      {
        "name": "Image position",
        "type": "Dropdown",
        "description": "Defines how the image is positioned.",
        "options": [
          {
            "name": "Left top",
            "class": "position-left-top"
          },
          {
            "name": "Left center",
            "class": "position-left-center"
          },
          {
            "name": "Left bottom",
            "class": "position-left-bottom"
          },
          {
            "name": "Right top",
            "class": "position-right-top"
          },
          {
            "name": "Right center",
            "class": "position-right-center"
          },
          {
            "name": "Right bottom",
            "class": "position-right-bottom"
          },
          {
            "name": "Center top",
            "class": "position-center-top"
          },
          {
            "name": "Center center",
            "class": "position-center-center"
          },
          {
            "name": "Center bottom",
            "class": "position-center-bottom"
          }
        ]
      }
    ],
    "Label": [
      {
        "name": "Style",
        "type": "Dropdown",
        "description": "Change the appearance of a label.",
        "options": [
          {
            "name": "Brand Default",
            "oldNames": [
              "Default"
            ],
            "class": "label-default"
          },
          {
            "name": "Brand Primary",
            "oldNames": [
              "Primary"
            ],
            "class": "label-primary"
          },
          {
            "name": "Brand Inverse",
            "oldNames": [
              "Inverse"
            ],
            "class": "label-inverse"
          },
          {
            "name": "Brand Info",
            "oldNames": [
              "Info"
            ],
            "class": "label-info"
          },
          {
            "name": "Brand Success",
            "oldNames": [
              "Success"
            ],
            "class": "label-success"
          },
          {
            "name": "Brand Warning",
            "oldNames": [
              "Warning"
            ],
            "class": "label-warning"
          },
          {
            "name": "Brand Danger",
            "oldNames": [
              "Danger"
            ],
            "class": "label-danger"
          }
        ]
      }
    ],
    "DynamicText": [
      {
        "name": "Font Weight",
        "oldNames": [
          "Weight"
        ],
        "type": "Dropdown",
        "description": "Emphasize the text with a heavier or lighter font weight",
        "options": [
          {
            "name": "Light",
            "class": "text-light"
          },
          {
            "name": "Normal",
            "class": "text-normal"
          },
          {
            "name": "Semibold",
            "class": "text-semibold"
          },
          {
            "name": "Bold",
            "class": "text-bold"
          }
        ]
      },
      {
        "name": "Color",
        "type": "Dropdown",
        "description": "Change the color of text.",
        "options": [
          {
            "name": "Header color",
            "class": "text-header"
          },
          {
            "name": "Detail color",
            "class": "text-detail"
          },
          {
            "name": "Brand Default",
            "oldNames": [
              "Default"
            ],
            "class": "text-default"
          },
          {
            "name": "Brand Primary",
            "oldNames": [
              "Primary"
            ],
            "class": "text-primary"
          },
          {
            "name": "Brand Inverse",
            "oldNames": [
              "Inverse"
            ],
            "class": "text-inverse"
          },
          {
            "name": "Brand Info",
            "oldNames": [
              "Info"
            ],
            "class": "text-info"
          },
          {
            "name": "Brand Success",
            "oldNames": [
              "Success"
            ],
            "class": "text-success"
          },
          {
            "name": "Brand Warning",
            "oldNames": [
              "Warning"
            ],
            "class": "text-warning"
          },
          {
            "name": "Brand Danger",
            "oldNames": [
              "Danger"
            ],
            "class": "text-danger"
          }
        ]
      },
      {
        "name": "Background color",
        "type": "Dropdown",
        "description": "Change the background color.",
        "options": [
          {
            "name": "Default",
            "class": "badge-default"
          },
          {
            "name": "Primary",
            "class": "badge-primary"
          },
          {
            "name": "Inverse",
            "class": "badge-inverse"
          },
          {
            "name": "Info",
            "class": "badge-info"
          },
          {
            "name": "Success",
            "class": "badge-success"
          },
          {
            "name": "Warning",
            "class": "badge-warning"
          },
          {
            "name": "Danger",
            "class": "badge-danger"
          }
        ]
      },
      {
        "name": "Alignment",
        "type": "Dropdown",
        "description": "Align the text.",
        "options": [
          {
            "name": "Left",
            "class": "text-left d-block"
          },
          {
            "name": "Center",
            "class": "text-center d-block"
          },
          {
            "name": "Right",
            "class": "text-right d-block"
          }
        ]
      },
      {
        "name": "Transform",
        "type": "Dropdown",
        "description": "Change the letter case of the text.",
        "options": [
          {
            "name": "Lowercase",
            "class": "text-lowercase"
          },
          {
            "name": "Uppercase",
            "class": "text-uppercase"
          },
          {
            "name": "Capitalize",
            "class": "text-capitalize"
          }
        ]
      },
      {
        "name": "Wrap options",
        "type": "Dropdown",
        "description": "Break long words and sentences into multiple lines.",
        "options": [
          {
            "name": "Wrap",
            "class": "text-break"
          },
          {
            "name": "No Wrap",
            "class": "text-nowrap"
          }
        ]
      }
    ]
  }
}