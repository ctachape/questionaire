// This file was generated by Mendix Studio Pro.
//
// WARNING: Code you write here will be lost the next time you deploy the project.

package covid.proxies.microflows;

import java.util.HashMap;
import java.util.Map;
import com.mendix.core.Core;
import com.mendix.core.CoreException;
import com.mendix.systemwideinterfaces.MendixRuntimeException;
import com.mendix.systemwideinterfaces.core.IContext;

public class Microflows
{
	// These are the microflows for the Covid module
	public static void aCT_Submit(IContext context, covid.proxies.ProfileInfo _profileInfo)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		params.put("ProfileInfo", _profileInfo == null ? null : _profileInfo.getMendixObject());
		Core.microflowCall("Covid.ACT_Submit").withParams(params).execute(context);
	}
	public static boolean afterStartUp(IContext context)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		return (java.lang.Boolean) Core.microflowCall("Covid.AfterStartUp").withParams(params).execute(context);
	}
	public static void documentDownload(IContext context, covid.proxies.Document _document, covid.proxies.DataEntry _dataEntry)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		params.put("Document", _document == null ? null : _document.getMendixObject());
		params.put("DataEntry", _dataEntry == null ? null : _dataEntry.getMendixObject());
		Core.microflowCall("Covid.DocumentDownload").withParams(params).execute(context);
	}
	public static boolean documentUpload(IContext context, covid.proxies.Document _document)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		params.put("Document", _document == null ? null : _document.getMendixObject());
		return (java.lang.Boolean) Core.microflowCall("Covid.DocumentUpload").withParams(params).execute(context);
	}
	public static void genYesNo(IContext context)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		Core.microflowCall("Covid.genYesNo").withParams(params).execute(context);
	}
	public static void getStarted(IContext context, covid.proxies.ProfileInfo _profileInfo)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		params.put("ProfileInfo", _profileInfo == null ? null : _profileInfo.getMendixObject());
		Core.microflowCall("Covid.GetStarted").withParams(params).execute(context);
	}
	public static void getStore(IContext context, java.lang.String _store)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		params.put("store", _store);
		Core.microflowCall("Covid.GetStore").withParams(params).execute(context);
	}
	public static void startApplication(IContext context)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		Core.microflowCall("Covid.StartApplication").withParams(params).execute(context);
	}
	public static void submitApplication(IContext context, covid.proxies.DataEntry _form)
	{
		Map<java.lang.String, Object> params = new HashMap<>();
		params.put("Form", _form == null ? null : _form.getMendixObject());
		Core.microflowCall("Covid.SubmitApplication").withParams(params).execute(context);
	}
}